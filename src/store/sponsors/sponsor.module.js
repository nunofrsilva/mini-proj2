import {
  ADD_SPONSOR,
  EDIT_SPONSOR,
  REMOVE_SPONSOR,
  SET_MESSAGE,
  SET_SPONSORS
} from "@/store/sponsors/sponsor.constants";

const state = {
  sponsors: [],
  message: ""
};

const getters = {
  getSponsors: state => state.sponsors,
  getSponsorById: state => id => {
    return state.sponsors.find(sponsor => sponsor._id === id);
  },
  getMessage: state => state.message
};

const actions = {
  [ADD_SPONSOR]: ({ commit }, payload) => {
    payload._id = UUIDGeneratorBrowser();
    payload.createdDate = new Date().toISOString().slice(0, 10);
    commit(SET_SPONSORS, [...state.sponsors, payload]);
  },
  [REMOVE_SPONSOR]: ({ commit }, id) => {
    commit(SET_SPONSORS, state.sponsors.filter(sponsor => sponsor._id !== id))
  },
  [EDIT_SPONSOR]: ({ commit }, payload) => {
    const sponsorIdx = state.sponsors.findIndex(sponsor => sponsor._id === payload.id);
    state.sponsors[sponsorIdx] = payload;
    commit(SET_SPONSORS, [...state.sponsors]);
  },
};

const UUIDGeneratorBrowser = () =>
  ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
    (c ^ (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))).toString(16)
  );

export const mutations = {
  [SET_SPONSORS]: (state, sponsors) => {
    state.sponsors = sponsors;
  },
  [SET_MESSAGE]: (state, message) => {
    state.message = message;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}