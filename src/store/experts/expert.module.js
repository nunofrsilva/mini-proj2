import { ADD_EXPERT, EDIT_EXPERT, REMOVE_EXPERT, SET_MESSAGE, SET_EXPERTS } from "@/store/experts/expert.constants";

const state = {
  experts: [],
  message: ""
};

const getters = {
  getExperts: state => state.experts,
  getExpertById: state => id => {
    return state.experts.find(expert => expert._id === id);
  },
  getMessage: state => state.message
};

const actions = {
  [ADD_EXPERT]: ({ commit }, payload) => {
    payload._id = UUIDGeneratorBrowser();
    payload.createdDate = new Date().toISOString().slice(0, 10);
    commit(SET_EXPERTS, [...state.experts, payload]);
  },
  [REMOVE_EXPERT]: ({ commit }, id) => {
    commit(SET_EXPERTS, state.experts.filter(expert => expert._id !== id))
  },
  [EDIT_EXPERT]: ({ commit }, payload) => {
    const expertIdx = state.experts.findIndex(expert => expert._id === payload.id);
    state.experts[expertIdx] = payload;
    commit(SET_EXPERTS, [...state.experts]);
  }
};

const UUIDGeneratorBrowser = () =>
([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
  (c ^ (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))).toString(16)
);

export const mutations = {
  [SET_EXPERTS]: (state, experts) => {
    state.experts = experts;
  },
  [SET_MESSAGE]: (state, message) => {
    state.message = message;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}